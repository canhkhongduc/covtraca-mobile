import React, {Component} from 'react';
import {connect} from 'react-redux';
import { StyleSheet, AsyncStorage, View, Image, Text, ImageBackground } from 'react-native';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import {Button} from 'react-native-elements';
import commonStyles from '../../constants/commonStyles';
import Onboarding from 'react-native-onboarding-swiper';
import Colors from '../../constants/Colors';
import Layout from "../../constants/Layout";
import LogoBar from '../../components/LogoBar';
import * as Location from 'expo-location';
import i18n from '../../localization/i18n';
import Constants from 'expo-constants';
import {getDeviceByUid, registerDevice} from '../../actions/device';

const width = Layout.window.width;

class OnBoardingScreen extends Component {

	componentDidMount = async() => {
		let { status } = await Location.requestPermissionsAsync();
		if (status !== 'granted') {
			console.error('Permission to access location was denied');
		}
		const deviceId = await AsyncStorage.getItem('deviceId');
		if(!deviceId) {
			await this.props.getDeviceByUid(Constants.deviceId);
			if(!this.props.device.device) {
				const device = {
					uid: Constants.deviceId,
					name: Constants.deviceName
				}
				await this.props.registerDevice(device);
				const response = this.props.device.device;
				if(response.id) {
					await AsyncStorage.setItem('deviceId', response.id.toString());
				}
			} else {
				await AsyncStorage.setItem('deviceId', this.props.device.device.id.toString());
			}
		}
		
	}
	
	onPressNext = async() => {
    this.props.navigation.navigate('Reporting');
    //update API
	}
	
	render() {
		return (
			<View style={styles.container}>
				<LogoBar />
				<View style={{flex: 1}}>
					<ImageBackground
						source={require('../../assets/images/background.png')}
						style={styles.backgroundImage}
					/>
					<Text style={styles.backgroundImageLabelTextFirst}>{i18n.t('onboarding.firstSlogan')}</Text>
					<Text style={styles.backgroundImageLabelTextSecond}>{i18n.t('onboarding.secondSlogan')}</Text>
				</View>
				
				<View style={{flex: 1.5}}>
					<Text style={styles.labelText}>{i18n.t('onboarding.appDescription')}</Text>
					<Button
						title={i18n.t('onboarding.reportToday')}
						titleStyle={styles.buttonTitleDefaultStyle}
						buttonStyle={styles.buttonDefaultStyle}
						containerStyle={styles.formSaveButtonContainer}
						onPress={this.onPressNext}
					/>
				</View>
      </View>
		);
	}
}

const mapStateToProps = state => ({
  device: state.device,
});

export default connect(mapStateToProps, {getDeviceByUid, registerDevice})(OnBoardingScreen);

const styles = StyleSheet.create({
	container: {
		display: 'flex',
		height: '100%'
	},
	logoBar: {
		height: 75,
		width: width,
		backgroundColor: Colors.tintColor,
		flexDirection: 'row'
	},
	logoHeader: {
		flex: 1,
		marginTop: 20,
		marginRight: 10,
		resizeMode: 'contain',
		justifyContent: 'flex-end'
	},
	drawerMenuIcon: {
		width: 25,
		height: 25,
		marginTop: 30,
		resizeMode: 'contain',
		marginLeft: 10
	},
	backgroundImage: {
		flex: 1,
		resizeMode: 'contain'
	},
	backgroundImageLabelTextFirst: {
		position: 'absolute',
		...commonStyles.fontRalewayBold,
		fontSize: RFPercentage(2),
		marginTop: 70,
		marginLeft: width * 0.45,
	},
	backgroundImageLabelTextSecond: {
		position: 'absolute',
		...commonStyles.fontRalewayBold,
		fontSize: RFPercentage(2),
		marginTop: 110,
		marginLeft: width * 0.5,
	},
	labelText: {
		...commonStyles.fontRalewaySemiBold,
		fontSize: 16,
		marginTop: 40,
		marginLeft: '7%',
		marginRight: '7%',
		textAlign: 'center',
		lineHeight: 25
	},
	buttonTitleDefaultStyle: {
		...commonStyles.fontRalewaySemiBold,
    fontSize: 14,
	},
	buttonDefaultStyle: {
		paddingVertical: 4,
    width: 300,
    height: 50,
    borderRadius: 20,
    backgroundColor: Colors.primaryColor,
  },
	formSaveButtonContainer: {
		marginTop: 80,
    marginBottom: 10,
    alignItems: 'center',
	}
});

