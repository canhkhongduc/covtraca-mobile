import React from 'react';
import Colors from '../constants/Colors';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import commonStyles from '../constants/commonStyles';
import {Icon} from 'react-native-elements';
import {TouchableHighlight} from 'react-native';
import ReportScreen from './MainScreens/ReportScreen';
import CompleteReportScreen from './MainScreens/CompleteReportScreen';
import LocalMapReportScreen from './MainScreens/LocalMapReportScreen';
import GlobalMapReportScreen from './MainScreens/GlobalMapReportScreen';
import OnBoardingScreen from './OnBoardingScreens/OnBoardingScreen';
import i18n from '../localization/i18n';

const HomeStack = createStackNavigator(
	{
    Home: {
      screen: OnBoardingScreen,
      navigationOptions: ({navigation}) => ({
        headerTitle: null,
        headerBackTitle: false,
        headerTransparent: true,
        headerTintColor: Colors.primaryColor,
        headerTitleStyle: {
          ...commonStyles.fontRalewayBold,
          fontSize: 18
        }
      })
    },
    Reporting: {
      screen: ReportScreen,
      navigationOptions: ({navigation}) => ({
        headerTitle: i18n.t('bottomMenu.reportNow'),
        headerLeft: null,
        headerBackTitle: false,
        headerTransparent: false,
        headerTintColor: Colors.primaryColor,
        headerTitleStyle: {
          ...commonStyles.fontRalewayBold,
          fontSize: 18,
        }
      })
    },
    CompleteReport: {
      screen: CompleteReportScreen,
      navigationOptions: ({navigation}) => ({
        headerTitle: i18n.t('reporting.thankYou'),
        headerLeft: null,
        headerBackTitle: false,
        headerTransparent: false,
        headerTintColor: Colors.primaryColor,
        headerTitleStyle: {
          ...commonStyles.fontRalewayBold,
          fontSize: 18
        }
      })
    } 
    
	},
	{
		initialRouteName: 'Home'
	}
);

HomeStack.navigationOptions = ({navigation}) => {
	const tabBarLabel = i18n.t('bottomMenu.home');
	const tabBarIcon = ({focused}) => (
    <Icon
      type="material"
			name="home"
			size={27}
			color={focused ? Colors.primaryColor : Colors.lightGrey}
		/>
	)
	const tabBarVisible = navigation.state.index === 0 ? true : false;
	return {tabBarLabel, tabBarIcon, tabBarVisible};
}

const GlobalMapStack = createStackNavigator(
	{
    Global: {
      screen: GlobalMapReportScreen,
      navigationOptions: ({navigation}) => ({
        headerTitle: i18n.t('bottomMenu.globalMap'),
        headerBackTitle: true,
        headerTransparent: false,
        headerTintColor: Colors.primaryColor,
        headerTitleStyle: {
          ...commonStyles.fontRalewayBold,
          fontSize: 18
        }
      })
    } 
    
	}
);

GlobalMapStack.navigationOptions = ({navigation}) => {
	const tabBarLabel = i18n.t('bottomMenu.globalMap');
	const tabBarIcon = ({focused}) => (
    <Icon
      type="material-community"
			name="earth"
			size={27}
			color={focused ? Colors.primaryColor : Colors.lightGrey}
		/>
	)
	const tabBarVisible = navigation.state.index === 0 ? true : false;
	return {tabBarLabel, tabBarIcon, tabBarVisible};
}

const LocalMapStack = createStackNavigator(
	{
		Home: {
      screen: LocalMapReportScreen,
      navigationOptions: ({navigation}) => ({
        headerTitle: i18n.t('bottomMenu.localMap'),
        headerBackTitle: true,
        headerTransparent: false,
        headerTintColor: Colors.primaryColor,
        headerTitleStyle: {
          ...commonStyles.fontRalewayBold,
          fontSize: 18
        }
      })
    } 
	}
);

LocalMapStack.navigationOptions = {
	tabBarLabel: i18n.t('bottomMenu.localMap'),
	tabBarIcon: ({ focused }) => (
    <Icon
      type="entypo"
			name="location"
			size={27}
			color={focused ? Colors.primaryColor : Colors.lightGrey}
		/>
	),
};

export default createBottomTabNavigator(
	{
    HomeStack,
		GlobalMapStack,
		LocalMapStack
	},
	{
		defaultNavigationOptions: {
			tabBarOptions: {
				activeTintColor: Colors.primaryColor,
				inactiveTintColor: Colors.lightGrey,
				labelStyle: [commonStyles.fontRalewayRegular, {fontSize: 10}],
				style: {
					height: 56,
					paddingTop: 6,
					paddingBottom: 3,
				}
			}
		}
	}
);