import React, { useState } from 'react';
import {StyleSheet, View, Text, TouchableHighlight, Image, AsyncStorage, TouchableOpacity} from 'react-native';
import Colors from '../../constants/Colors';
import {Button, Icon} from 'react-native-elements';
import * as Progress from 'react-native-progress';
import commonStyles from '../../constants/commonStyles';
import {connect} from 'react-redux';
import { setTreatment } from '../../actions/symptom';
import Constants from 'expo-constants';
import i18n from '../../localization/i18n';
import Layout from "../../constants/Layout";

const height = Layout.window.height;
class CompleteReportScreen extends React.Component {
  onPressHome = async() => {
    this.props.navigation.navigate('Home');
  }

  render() {
    return (
      <View style={styles.container}>
        <Progress.Bar progress={1} width={300} color={Colors.primaryColor} style={{marginTop: 50}}/>
        <Icon
          name={'done'}
          size={40}
          color={Colors.white}
          containerStyle={styles.editIconContainer}
        />
        <Text style={styles.textContent}>{i18n.t('reporting.thankYou1')}</Text>
        <Text style={styles.textContent}>{i18n.t('reporting.thankYou2')}</Text>
        <TouchableOpacity  onPress={this.onPressHome}>
          <Icon
            name={'home'}
            size={40}
            color={Colors.white}
            containerStyle={styles.homeIconContainer}
          />
        </TouchableOpacity>
        
        <Text style={styles.domainText}>covtraca.org</Text>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  symptom: state.symptom,
});

export default connect(mapStateToProps, {setTreatment})(CompleteReportScreen)

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  headerContentText: {
    ...commonStyles.fontMontserratBold,
    fontSize: 15,
    marginTop: 40
  },
  buttonTitleDefaultStyle: {
		...commonStyles.fontRalewaySemiBold,
    fontSize: 14,
	},
	buttonDefaultStyle: {
		paddingVertical: 4,
    width: 300,
    height: 50,
    borderRadius: 20,
    backgroundColor: Colors.primaryColor,
  },
	formSaveButtonContainer: {
		marginTop: 30,
    marginBottom: 10,
    alignItems: 'center',
  },
  domainText: {
    color: Colors.primaryColor,
    fontSize: 22,
    bottom: 0
  },
  editIconContainer: {
    backgroundColor: Colors.primaryColor,
    borderRadius: 50,
    padding: 4,
    marginTop: 30
  },
  textContent: {
    ...commonStyles.fontRalewayRegular,
    fontSize: 15,
    marginTop: 40,
    width: '80%',
    textAlign: 'center',
    lineHeight: 25
  },
  homeIconContainer: {
    backgroundColor: Colors.primaryColor,
    borderRadius: 50,
    padding: 4,
    marginTop: 50
  }
});