import React, {Component} from 'react';
import {connect} from 'react-redux';
import { StyleSheet, View, Dimensions, Text, TouchableOpacity, ActivityIndicator, Animated, Platform } from 'react-native';
import * as Location from 'expo-location'
import MapView, { PROVIDER_GOOGLE, AnimatedRegion} from 'react-native-maps';
import { Icon } from 'react-native-elements';
import Colors from '../../constants/Colors';
import {getCovidLocalData} from '../../actions/covidSummary';
import _ from "lodash";
import {purifyData} from "../../utils/handleCovidSummary";
import * as Localization from 'expo-localization';
import i18n from '../../localization/i18n';
import { formatNumber } from '../../utils/formatNumber';

class LocalMapReportScreen extends Component {
  state = {
    location: {}
  }
  componentDidMount = async() => {
    await this.props.getCovidLocalData(Localization.locale.slice(3, 5));
    if(this.props.summary.local) {
      let records = this.props.summary.local;
      if(records[records.length - 1].CityCode === "") {
        this.setState({countryRecord: records[records.length - 1]});
      }
      this.setState({records});
    }

    let location = await Location.getCurrentPositionAsync({});
    this.setState({
      initialRegion: {
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
        latitudeDelta: 25,
        longitudeDelta: 25
      }
    });
  }
  onPressBack= () => {
    this.props.navigation.navigate('OnBoarding');
  }

  setMarkerStyle = (total) => {
    return {
      borderRadius: this.markerSize(total) / 2,
      backgroundColor: Colors.red,
      width: this.markerSize(total),
      height: this.markerSize(total)
    }
    
  }
  customizeDescription = (country) => {
    if(Platform.OS === 'android') {
      return `${i18n.t('map.confirmed')}: ${formatNumber(country.Confirmed)} ${i18n.t('map.death')}: ${formatNumber(country.Deaths)} ${i18n.t('map.recovered')}: ${formatNumber(country.Recovered)}`;
    } else {
      return `${i18n.t('map.confirmed')}: ${formatNumber(country.Confirmed)}\n${i18n.t('map.death')}: ${formatNumber(country.Deaths)}\n${i18n.t('map.recovered')}: ${formatNumber(country.Recovered)}`;
    }
  }

  goToInitialLocation = () => {
    let initialRegion = Object.assign({}, this.state.initialRegion);
    initialRegion["latitudeDelta"] = 0.005;
    initialRegion["longitudeDelta"] = 0.005;
    this.mapView.animateToRegion(initialRegion, 2000);
  }

  markerSize = (total) => {
    if(total < 100) {
      return 8;
    } else if(total >= 100 && total < 1000) {
      return 16;
    } else if(total >= 1000 && total < 10000) {
      return 20;
    } else {
      return 35;
    }
  }
	render() {
    console.log(this.state.countryRecord);
		return (
      <View style={styles.container}>
        {!this.state.initialRegion && (
          <ActivityIndicator size="large" color={Colors.primaryColor} style={{marginTop: 15}}/>
        )}
        {this.state.initialRegion && (this.state.records.length > 0) && (
          <MapView 
            style={styles.mapStyle}
            provider={PROVIDER_GOOGLE}
            initialRegion={this.state.initialRegion}
            showsUserLocation={true}
          >
          {this.state.countryRecord ? (
            <MapView.Marker
              coordinate={{latitude: parseFloat(this.state.countryRecord.Lat),
              longitude: parseFloat(this.state.countryRecord.Lon)}}
              title={this.state.countryRecord.Country}
              description={this.customizeDescription(this.state.countryRecord)}
            >
              <TouchableOpacity style={this.setMarkerStyle(this.state.countryRecord.Confirmed)}>
              </TouchableOpacity>
            </MapView.Marker>
          ) : (
            this.state.records.map((record, index) => {
              if(record.Confirmed > 0) 
                return (
                  <MapView.Marker
                    coordinate={{latitude: parseFloat(record.Lat),
                    longitude: parseFloat(record.Lon)}}
                    title={record.Province}
                    description={this.customizeDescription(record)}
                    key={index}
                  >
                    <TouchableOpacity style={this.setMarkerStyle(record.Confirmed)}>
                    </TouchableOpacity>
                  </MapView.Marker>
                )
            })
          )}
          </MapView>
        )}
      </View>
		);
	}
}

const mapStateToProps = state => ({
  summary: state.summary
});

export default connect(mapStateToProps, {getCovidLocalData})(LocalMapReportScreen);

const styles = StyleSheet.create({
	container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mapStyle: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  },
  backIconContainer: {
    backgroundColor: Colors.primaryColor,
    borderRadius: 50,
    padding: 4,
  },
  markerContainer: {
    borderRadius: 4,
    backgroundColor: Colors.red,
  },
});
