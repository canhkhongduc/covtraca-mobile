import React, {Component} from 'react';
import {connect} from 'react-redux';
import { StyleSheet, View, Dimensions, Text, TouchableOpacity, ActivityIndicator, Animated, TouchableHighlight, Platform } from 'react-native';
import * as Location from 'expo-location'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { Icon } from 'react-native-elements';
import Colors from '../../constants/Colors';
import {getCovidData} from '../../actions/covidSummary';
import _ from "lodash";
import {purifyData} from "../../utils/handleCovidSummary";
import commonStyles from '../../constants/commonStyles';
import i18n from '../../localization/i18n';
import {formatNumber} from '../../utils/formatNumber';

class GlobalMapReportScreen extends Component {

  state = {
    location: {}
  }
  componentDidMount = async() => {
    await this.props.getCovidData();
    if(this.props.summary.summary) {
      let countries = purifyData(this.props.summary.summary.Countries);
      this.setState({countries});
    }

    let location = await Location.getCurrentPositionAsync({});
    this.setState({location});
  }
  onPressBack= () => {
    this.props.navigation.navigate('OnBoarding');
  }

  setMarkerStyle = (total) => {
    return {
      borderRadius: this.markerSize(total) / 2,
      backgroundColor: Colors.red,
      width: this.markerSize(total),
      height: this.markerSize(total)
    }
    
  }

  markerSize = (total) => {
    if(total < 1000) {
      return 8;
    } else if(total >= 1000 && total < 10000) {
      return 16;
    } else if(total >= 10000 && total < 100000) {
      return 20;
    } else {
      return 35;
    }
  }
  customizeDescription = (country) => {
    if(Platform.OS === 'android') {
      return `${i18n.t('map.confirmed')}: ${formatNumber(country.TotalConfirmed)} ${i18n.t('map.death')}: ${formatNumber(country.TotalDeaths)} ${i18n.t('map.recovered')}: ${formatNumber(country.TotalRecovered)}`;
    } else {
      return `${i18n.t('map.confirmed')}: ${formatNumber(country.TotalConfirmed)}\n${i18n.t('map.death')}: ${formatNumber(country.TotalDeaths)}\n${i18n.t('map.recovered')}: ${formatNumber(country.TotalRecovered)}`;
    }
  }
	render() {
    console.log(this.state);
    const countries = this.state.countries;
		return (
      <View style={styles.container}>
        {!this.state.location.coords && (
          <ActivityIndicator size="large" color={Colors.primaryColor} style={{marginTop: 15}}/>
        )}
        {this.state.location.coords && countries && (
          <MapView 
            style={styles.mapStyle}
            provider={PROVIDER_GOOGLE}
            region={{
              latitude: this.state.location.coords.longitude,
              longitude: this.state.location.coords.latitude,
              latitudeDelta: 0.015 * 10000,
              longitudeDelta: 0.0121 * 10000
            }}
          >
            {countries.map((country, index) => {
              if(country.TotalConfirmed > 0) 
                return (
                  <MapView.Marker
                    coordinate={{latitude: country.coordinates.latitude,
                    longitude: country.coordinates.longitude}}
                    title={country.Country}
                    description={this.customizeDescription(country)}
                    key={index}
                  >
                    <TouchableOpacity onPress={this.onPressBack} style={this.setMarkerStyle(country.TotalConfirmed)}>
                    </TouchableOpacity>
                  </MapView.Marker>
                )
            })
            }
            
          </MapView>
        )}
      </View>
		);
	}
}

const mapStateToProps = state => ({
  summary: state.summary
});

export default connect(mapStateToProps, {getCovidData})(GlobalMapReportScreen);

const styles = StyleSheet.create({
	container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mapStyle: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  },
  backIconContainer: {
    backgroundColor: Colors.primaryColor,
    borderRadius: 50,
    padding: 4,
  },
  markerContainer: {
    borderRadius: 4,
    backgroundColor: Colors.red,
  },
});

