import React, { useState } from 'react';
import {
  StyleSheet, Picker, 
  View, Text, Image, 
  ActivityIndicator, 
  ScrollView, AsyncStorage, 
  TouchableOpacity, 
  ToastAndroid, Alert,
} from 'react-native';
import {Icon} from 'react-native-elements';
import Colors from '../../constants/Colors';
import RadioForm from 'react-native-simple-radio-button';
import {Button} from 'react-native-elements';
import commonStyles from '../../constants/commonStyles';
import {connect} from 'react-redux';
import { setIsTested } from '../../actions/symptom';
import Layout from "../../constants/Layout";
import * as Progress from 'react-native-progress';
import {registerDevice} from '../../actions/device';
import {getQuestions} from '../../actions/question';
import {saveReport} from '../../actions/report';
import { booleanRadioProps } from '../../constants/RadioProps';
import SelectQuestion from '../../components/SelectQuestion';
import CheckBoxItem from '../../components/CheckBoxItem';
import i18n from '../../localization/i18n';
import * as Location from 'expo-location';
import Constants from 'expo-constants';

const width = Layout.window.width;

class ReportScreen extends React.Component {
  state = {
    questions: [],
    activeQuestionIndex: 0,
    symptoms: [],
    savingRecord: false,
  }

  componentDidMount = async() => {
    await this.props.getQuestions();
    await this.getDeviceIdFromAsyncStorage();
    this.setState({
      questions: this.props.question.questions
    });
  }

  onPressNext = async() => {
    if(this.state.activeQuestionIndex === (this.state.questions.length - 1)) {
      this.completeReport();
    } else if(this.state.activeQuestionIndex === 2 && this.state["3"] === false) {
      this.completeReport();
    } else {
      this.setState({
        activeQuestionIndex: this.state.activeQuestionIndex + 1
      });
    }
  }

  onPressBack = () => {
    this.setState({
      activeQuestionIndex: this.state.activeQuestionIndex - 1
    });
  }

  onPressHome = () => {
    Alert.alert(
      null,
      i18n.t('reporting.stopReportingAlert'),
      [
        {
          text: i18n.t('reporting.no'),
          style: "cancel"
        },
        {
          text: i18n.t('reporting.yes'),
          onPress: () => this.props.navigation.navigate('Home')
        }
      ],
      { cancelable: true }
    );
  }

  completeReport = async() => {
    const deviceId = await AsyncStorage.getItem('deviceId');
    let location = await Location.getCurrentPositionAsync({});

    let answer = {...this.state};
    delete answer.activeQuestionIndex;
    delete answer.questions; 
    delete answer.savingRecord;
    const symptomQuestion = this.findSymtomQuestion(this.state.questions);
    if(symptomQuestion) {
      answer[symptomQuestion.id] = Object.entries(answer.symptoms);
      delete answer.symptoms;
    }
    if(Object.keys(answer).length > 1) {
      this.setState({savingRecord: true});
      const report = {
        answer: JSON.stringify(answer),
        device_id: deviceId,
        lat: location.coords.latitude,
        long: location.coords.longitude
      }
      await this.props.saveReport(report);
      if(this.props.report.report.id) {
        this.props.navigation.navigate('CompleteReport');
        this.setState({savingRecord: false, todayReported: true});
      }
    } else {
      Alert.alert(i18n.t('reporting.emptyAnswerAlert'));
    }
  }

  getDeviceIdFromAsyncStorage = async() => {
    const deviceId = await AsyncStorage.getItem('deviceId');
		if(!deviceId) {
			const device = {
				uid: Constants.deviceId,
				name: Constants.deviceName
      }
      console.log(device);
			await this.props.registerDevice(device);
			const response = this.props.device.device;
			if(response.id) {
        await AsyncStorage.setItem('deviceId', response.id.toString());
        return response.id;
      }      
    }
  }
  

  findSymtomQuestion = (questions) => {
    for(let i = 0; i < questions.length; i++) {
      if(questions[i].state_name === 'symptoms') {
        return questions[i];
      }
    }
    return null;
  }

  onChangeValue = (stateName, value) => {
    this.setState({[stateName]: value});
  }

  onCheckboxChangeValue = (statename) => {
    let symptoms = this.state.symptoms;
    symptoms[statename] = !symptoms[statename];
    this.setState({symptoms});
  }
  
  render() {
    let progress = 0;
    let activeQuestion = this.state.questions[this.state.activeQuestionIndex];
    if(activeQuestion) {
      progress = activeQuestion.id / (this.state.questions.length + 1);
      if(typeof activeQuestion.options === 'string') {
        activeQuestion.options = JSON.parse(activeQuestion.options);
      }
    }
    return (
      <ScrollView 
        showsVerticalScrollIndicator={false}
        style={styles.container}
      >
        <View style={styles.mainContent}>
          <Text style={styles.headerContentText}>{i18n.t('reporting.header')}</Text>
          <Progress.Bar progress={progress} width={300} color={Colors.primaryColor} style={{marginTop: 10}}/>
          {!activeQuestion && (
            <ActivityIndicator size="large" color={Colors.primaryColor} style={{marginTop: 15}}/>
          )}
          {activeQuestion && (activeQuestion.type === 'select') && (
            <SelectQuestion 
              question={activeQuestion} 
              progress={progress}
              onChangeValue={this.onChangeValue}
              key={activeQuestion.id}
            />
          )}
          {activeQuestion && (activeQuestion.type === 'checkbox') && (
            activeQuestion.options.map((option, index) => (
              <CheckBoxItem 
                item={option}
                onChangeValue={this.onCheckboxChangeValue}
                checked={this.state.symptoms[option.id]}
                key={index}
              />
            ))
          )}
          <View style={{display: 'flex', flexDirection: 'row', width: '40%', justifyContent: 'center', alignItems: 'center'}}>
            {(this.state.activeQuestionIndex === 0) ? (
              <TouchableOpacity 
              onPress={this.onPressHome}
              style={{flex: 1, alignItems: 'center'}}
              >
                <Icon 
                  name='home'
                  size={30}
                  color={Colors.black}
                  containerStyle={styles.buttonIconContainer}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity 
              onPress={this.onPressBack}
              style={{flex: 1, alignItems: 'center'}}
              >
                <Icon 
                  name='arrow-back'
                  size={30}
                  color={Colors.black}
                  containerStyle={styles.buttonIconContainer}
                />
              </TouchableOpacity>
            )}
            
            <TouchableOpacity 
              onPress={this.onPressNext}
              style={{flex: 1, alignItems: 'center'}}
            >
              <Icon 
                name='arrow-forward'
                size={30}
                color={Colors.black}
                containerStyle={styles.buttonIconContainer}
              />
            </TouchableOpacity>
          </View>
          {this.state.savingRecord && (
            <ActivityIndicator size="large" color={Colors.primaryColor} style={styles.loadingOverlay}/>
          )}
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  device: state.device,
  question: state.question,
  report: state.report
});

export default connect(mapStateToProps, {registerDevice, getQuestions, saveReport})(ReportScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  mainContent: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  headerContentText: {
    ...commonStyles.fontMontserratBold,
    fontSize: 20,
    marginTop: 50,
    marginBottom: 10
  },
  questionText: {
    fontSize: 17,
		color: Colors.black,
    backgroundColor: 'transparent',
    marginTop: 30
  },
  picker: {
    height: 50,
    width: 150
  },
  buttonTitleDefaultStyle: {
		...commonStyles.fontRalewaySemiBold,
    fontSize: 14,
	},
	buttonDefaultStyle: {
		paddingVertical: 4,
    width: 300,
    height: 50,
    borderRadius: 20,
    backgroundColor: Colors.primaryColor,
  },
  secondaryButtonStyle: {
    paddingVertical: 4,
    width: 300,
    height: 50,
    borderRadius: 20,
    backgroundColor: Colors.secondaryColor,
  },
	formNextButtonContainer: {
		marginTop: 40,
    marginBottom: 10,
    alignItems: 'flex-end',
  },
  formBackButtonContainer: {
    marginTop: 20,
    alignItems: 'flex-end',
  },
  radioButtonContainer: {
    marginTop: 8
  },
  loadingOverlay: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonIconContainer: {
    backgroundColor: Colors.primaryColor,
    borderRadius: 45,
    padding: 10,
    marginTop: 25,
    marginBottom: 20,
    width: 50,
    height: 50
  }
});