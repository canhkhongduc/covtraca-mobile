const REGISTER_DEVICE = 'REGISTER_DEVICE';
const GET_DEVICE_BY_UID = 'GET_DEVICE_BY_UID';

export default {
  REGISTER_DEVICE,
  GET_DEVICE_BY_UID
};
