const DOMAIN = 'https://api.covtraca.org';
const COMMON_API_PATH = '/v1';
const QUESTION_API_PATH = '/questions';
const DEVICE_API_PATH = '/devices';
const REPORT_API_PATH = '/reports';

const COVID19_API = 'https://api.covid19api.com';
const SUMMARY = '/summary';
const LOCAL = '/live/country/';
const LOCAL_DAILY = '/country/'


// Question APIs
export const getAllQuestions = `${DOMAIN}${COMMON_API_PATH}${QUESTION_API_PATH}`;

//Device APIs
export const registerDevice = `${DOMAIN}${COMMON_API_PATH}${DEVICE_API_PATH}`;
export const getDeviceByUid = uid => `${DOMAIN}${COMMON_API_PATH}/getUID/${uid}`;

//Report APIs
export const saveReport = `${DOMAIN}${COMMON_API_PATH}${REPORT_API_PATH}`;

//Covid APIs
export const getCovidSummary = `${COVID19_API}${SUMMARY}`;
export const getCovidLocal = countryCode => `${COVID19_API}${LOCAL}${countryCode}`;
export const getCovidLocalDaily = countryCode => `${COVID19_API}${LOCAL_DAILY}${countryCode}`;

