export const booleanRadioProps = 
[
  {label: 'Yes', value: true },
  {label: 'No', value: false }
];

export const temperatureRadioProps = [
  {label: 'C', value: 'C' },
  {label: 'F', value: 'F' }
];