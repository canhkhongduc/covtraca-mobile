import { NavigationContainer } from '@react-navigation/native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import OnBoardingScreen from '../screens/OnBoardingScreens/OnBoardingScreen';
import MainScreenNavigator from '../screens/MainScreenNavigator';

export default createAppContainer(
    createSwitchNavigator(
      {
        Main: MainScreenNavigator
      },
      { initialRouteName: 'Main' }
    )
);
  