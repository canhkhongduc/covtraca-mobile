import _ from 'lodash';
import CountryCodes from './countryCodes.json';

export function purifyData(countries) {
  for(let i = 0; i < countries.length; i++) {
    let country = countries[i];
    let search_country = searchCountryByCode(countries[i].CountryCode);
    if(search_country) {
      countries[i].coordinates = {
        latitude: search_country.latlng[0],
        longitude: search_country.latlng[1]
      }
    }
  }
  return countries;
}

function searchCountryByCode(code) {
  for(let i = 0; i< CountryCodes.length; i++) {
    if(code === CountryCodes[i].country_code) {
      return CountryCodes[i];
    }
  }
  return null;
}