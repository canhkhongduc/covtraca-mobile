export default {
  onboarding: {
    firstSlogan: 'COVID-19の蔓延を阻止する',
    secondSlogan: '毎日1分かかる',
    appDescription: 'COVTRACAは、ウイルスと闘う人々がウイルスの蔓延を理解するのを支援するために、グローバルコミュニティによって開発されたグローバルヘルスアプリです。',
    reportToday: '元気であっても、今日報告してください'
  },
  reporting: {
    header: "COVID-19ステータス",
    yes: "はい",
    no: "いいえ",
    next: "次",
    back: "戻る",
    healthy: "元気",
    unhealthy: "悪い",
    isTested: "COVID-19のテストはありましたか?",
    isPositive: "COVID-19の検査で陽性でしたか?",
    currentHealth: "体調はどうですか?",
    highTemperature: "高温になりますか?",
    coughing: "継続的な咳がありますか?",
    soreThroat: "喉の痛みはありますか?",
    shortOfBreath: "息切れですか?",
    muscleOrJointPain: "筋肉や関節の痛みはありますか?",
    lostSmellOrTaste: "匂いや味覚を失いましたか?",
    hasFatigue: "疲れますか?",
    runnyNose: "鼻水はありますか?",
    thankYou: "ありがとうございました",
    thankYou1: "COVID-19の研究へのあなたの助けと重要な貢献をありがとうございました。",
    thankYou2: "私たちはCOVID-19の蔓延と戦うグローバルコミュニティです。 あなたの参加により、アプリケーションを拡大し、ウイルスの世界的な距離と封じ込めの支援を開始できます",
    done: "完了",
    viewMap: "地図を見る"
  },
  map: {
    confirmed: "Confirmed",
    recovered: "Recovered",
    death: "Deaths"
  },
  bottomMenu: {
    home: "Home",
    globalMap: "Global Map",
    localMap: "Local Map",
    reportNow: "Report now",
  }
};
