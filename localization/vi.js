export default {
  onboarding: {
    firstSlogan: 'Giúp ngăn chặn sự lây lan của COVID-19',
    secondSlogan: 'Dành 1 phút mỗi ngày',
    appDescription: 'COVTRACA là một ứng dụng y tế toàn cầu được phát triển bởi cộng đồng tình nguyện viên toàn cầu để hỗ trợ những người chống lại vi-rút hiểu được sự lây lan của nó',
    reportToday: 'Báo cáo ngay hôm nay, ngay cả khi bạn đang khỏe'
  },
  reporting: {
    header: "Trạng thái COVID-19",
    yes: 'Đúng',
    no: 'Sai',
    next: 'Tiếp',
    back: "Quay lại",
    healthy: "Bình thường",
    unhealthy: "Không khỏe",
    isTested: "Bạn đã xét nghiệp COVID-19?",
    isPositive: "Bạn có dương tính với COVID-19 không?",
    currentHealth: "Bạn đang cảm thấy thế nào?",
    highTemperature: "Bạn có nhiệt độ cao?",
    coughing: "Bạn có ho liên tục?",
    soreThroat: "Bạn có đau họng?",
    shortOfBreath: "Bạn thấy khó thở?",
    muscleOrJointPain: "Bạn thấy đau nhức xương khớp?",
    lostSmellOrTaste: "Bạn bị mất thính hoặc vị giác?",
    hasFatigue: "Bạn cảm thấy mệt mỏi?",
    runnyNose: "Bạn có bị chảy nước mũi?",
    thankYou: "CẢM ƠN BẠN",
    thankYou1: "Cảm ơn bạn đã giúp đỡ và đóng góp quan trọng cho nghiên cứu về COVID-19",
    thankYou2: "Chúng tôi là một cộng đồng toàn cầu chống lại sự lây lan của COVID-19. Sự tham gia của bạn sẽ cho phép chúng tôi phát triển ứng dụng của chúng tôi và bắt đầu hỗ trợ ngăn chặn và ngăn chặn virus toàn cầu",
    done: "Hoàn thành",
    viewMap: "Xem bản đồ"
  },
  map: {
    confirmed: "Người nhiễm",
    recovered: "Hồi phục",
    death: "Tử vong"
  },
  bottomMenu: {
    home: "Trang chủ",
    globalMap: "Toàn cầu",
    localMap: "Địa phương",
    reportNow: "Báo cáo",
  }
};
