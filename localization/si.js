export default {
  onboarding: {
    firstSlogan: 'COVID -19 ව්‍යාප්තවීම වලක්වාලිමට උපකාරී වන්න',
    secondSlogan: 'දිනපතා විනාඩියක් වැයකරන්න',
    appDescription: 'COVTRACA යනූ වෛරසය සමග සටන් කරන පිරිසට, එහි ව්‍යාප්තිය අවබෝධ කර ගැනීම සඳහා ෝලිය ප්‍රජාව විසින් සකස් කරන ලද ගොලීය සෞඛ්ය යෙදුමකි',
    reportToday: 'වාර්තා කරන්න, ඔබ මනා සෞඛ්‍ය  තත්වය ක සිටියත්'
  },
  reporting: {
    header: "COVID-19 status",
    yes: "ඔව්",
    no: "නැත",
    next: "මීළඟ",
    back: "ආපසු",
    healthy: "සාමාන්‍ය හොද සෞඛ්‍ය තත්වය",
    unhealthy: "එතරම් හොඳ නැත",
    isTested: "ඔබ COVID-19 පරීක්ෂණයක කර තිබෙනවා ද",
    isPositive: "ඔබට COVID-19 ෝග ලක්ෂණ පෙන්නුම් තිබූ නාද?",
    currentHealth: "ඔබගේ ශාරිරික සුවතා ව කෙසේ ද ?",
    highTemperature: "ඔබට අධික උණ ගතියක් දැනෙනවා ද",
    coughing: "ඔබට අලුතින් වර්ධනය වූ වියළි කැස්සක් තිබේද?",
    soreThroat: "ඔබට උගුරේ අමාරුවක් තිබේද?",
    shortOfBreath: "ඔබට ශ්වසන අපහසුතාවයෙන් පිඩා විඳින වාද ?",
    muscleOrJointPain: "ඔබට මාංශ පේශි හෝ සන්ධි වේදනාවක් තිබේද?",
    lostSmellOrTaste: "ඔබට සූවද හො රස පිලිබඳ දැනීම අහිමි විමක් දැනෙනවා ද",
    hasFatigue: "ඔබට අධික වෙහෙසක් හො තෙහෙට්ටුව ක් දැනෙනවා ද",
    runnyNose: "ඔබට නාසයෙන් දියර ගැලීමක් තිබෙනවා ද",
    thankYou: "ඔබට ස්තුතියි",
    thankYou1: "COVID-19 පිළිබඳ අධ්‍යයනය ට ඔබ දැක්වූ දායකත්වය  සහ සහයොගය අගය කරමු",
    thankYou2: "අපි COVID- 19 පැතිරීම වැළැක්වීමට පියවර ගන්නා ෝලීය ප්‍රරාවකි. ඔබගේ සහභාගිත්වය මගින් අපගේ යෙදුම වැඩි දියුණු කිරීමට සහ ෝලීය දුරස්ථභාවය සහ වෛරසය මැඩ පැවැත්වීමට දායක වීමට අප හට ඉඩ සැලසේ.",
    done: "නිමාව",
    viewMap: "සිතියම බලන්න"
  },
  map: {
    confirmed: "තහවුරු කර ඇත",
    recovered: "සුවය ලැබූ නඩු",
    death: "මරණ"
  },
  bottomMenu: {
    home: "Home",
    globalMap: "Global Map",
    localMap: "Local Map",
    reportNow: "Report now",
  }
};
