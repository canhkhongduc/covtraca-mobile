//Portugese
export default {
  onboarding: {
    firstSlogan: 'Ajude a impedir a propagação do COVID-19',
    secondSlogan: 'Gaste 1 minuto por dia',
    appDescription: 'COVTRACA é um aplicativo de saúde global desenvolvido pela comunidade global para ajudar aqueles que combatem o vírus a entender sua propagação',
    reportToday: 'Relate o seu dia, mesmo se você estiver bem'
  },
  reporting: {
    header: "COVID-19 status",
    yes: "Sim",
    no: "Não",
    next: "Próximo",
    back: "Voltar",
    healthy: "Tão saudável quanto o normal",
    unhealthy: "Não muito bem",
    isTested: "Você fez o teste para o COVID-19?",
    isPositive: "Você testou positivo para o COVID-19?",
    currentHealth: "Como você se sente fisicamente?",
    highTemperature: "Você está com a  temperatura corporal elevada (febre)?",
    coughing: "Você desenvolveu recentemente tosse seca?",
    soreThroat: "Você tem dor de garganta?",
    shortOfBreath: "Você está com falta de ar?",
    muscleOrJointPain: "Você está  com dores musculares ou nas articulações?",
    lostSmellOrTaste: "Você perdeu o olfato ou o paladar?",
    hasFatigue: "Você se sente cansado ou exausto?",
    runnyNose: "Você está com nariz escorrendo (coriza)?",
    thankYou: "Obrigado",
    thankYou1: "Obrigado pela sua ajuda e contribuição vital para o estudo do COVID-19",
    thankYou2: "Somos uma comunidade global que luta contra a disseminação do COVID-19. Sua participação nos permitirá aumentar nosso aplicativo e começar a ajudar no distanciamento e contenção globais do vírus",
    done: "Pronto",
    viewMap: "Veja o mapa"
  },
  map: {
    confirmed: "Confirmed",
    recovered: "Recovered",
    death: "Deaths"
  },
  bottomMenu: {
    home: "Home",
    globalMap: "Global Map",
    localMap: "Local Map",
    reportNow: "Report now",
  }
};