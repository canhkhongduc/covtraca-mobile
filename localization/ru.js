export default {
  onboarding: {
    firstSlogan: 'Помогите остановить распространение Коронавируса',
    secondSlogan: 'Это займёт 1 минуту',
    appDescription: 'Соvtraca это app Разработан мировым сообществом чтобы помочь понять его распространения',
    reportToday: 'Сообщите сегодня, даже если вы себя чувствуете нормально'
  },
  reporting: {
    header: "Статус Коронавируса",
    yes: "Да",
    no: "Нет",
    next: "Дальше",
    back: "Обратно",
    healthy: "Здоровье как обычно",
    unhealthy: "Не очень хорошо",
    isTested: "Вам сделали тест коронавируса?",
    isPositive: "У вас был положительный результат теста коронавируса?",
    currentHealth: "Как вы себя физически чувствуйте?",
    highTemperature: "У вас есть повышенная температура?",
    coughing: "У вас есть кашель который не уходит?",
    soreThroat: "У вас горло болит?",
    shortOfBreath: "У вас одышка есть?",
    muscleOrJointPain: "У вас есть боли в мышцах или суставах?",
    lostSmellOrTaste: "Вы потеряли свое обоняние или вкус?",
    hasFatigue: "Вы чувствуйте себя уставшими?",
    runnyNose: "У вас есть насморк?",
    thankYou: "Спасибо!",
    thankYou1: "Спасибо что помогли нам в изучение коронавируса.",
    thankYou2: "Мы - глобальное сообщество, борющееся с распространением COVID-19. Ваше участие позволит нам расширить наше приложение и начать помогать с глобальным удалением и сдерживанием вируса.",
    done: "Конец вопросов",
    viewMap: "Посмотреть карту"
  },
  map: {
    confirmed: "Confirmed",
    recovered: "Recovered",
    death: "Deaths"
  },
  bottomMenu: {
    home: "Home",
    globalMap: "Global Map",
    localMap: "Local Map",
    reportNow: "Report now",
  }
};
