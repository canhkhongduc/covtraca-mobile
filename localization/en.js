export default {
  onboarding: {
    firstSlogan: 'Help stop the spread of COVID-19',
    secondSlogan: 'Take 1 minute each day',
    appDescription: 'COVTRACA  is a global health app developed by the global community to assist those fighting the virus to understand its spread',
    reportToday: 'Report today, even if you\'re well'
  },
  reporting: {
    header: "COVID-19 status",
    yes: "Yes",
    no: "No",
    next: "Next",
    back: "Back",
    healthy: "As healthy as normal",
    unhealthy: "Not quite well",
    isTested: "Have you had a test for COVID-19?",
    isPositive: "Did you test positive for COVID-19?",
    currentHealth: "How do you feel physically?",
    highTemperature: "Do you have a high temperature?",
    coughing: "Do you have a newly developed dry cough?",
    soreThroat: "Do you have a sore throat?",
    shortOfBreath: "Are you short of breath?",
    muscleOrJointPain: "Do you have muscle or joint pain?",
    lostSmellOrTaste: "Have you lost your sense of smell or taste?",
    hasFatigue: "Do you feel tired or exhausted?",
    runnyNose: "Do you have a runny nose?",
    thankYou: "THANK YOU",
    thankYou1: "Thank you for your help and vital contribution to the study of COVID-19",
    thankYou2: "We are a global community fighting the spread of COVID-19. Your participation will allow us to grow our application and begin assisting with global distancing and containment of the virus",
    done: "Done",
    stopReportingAlert: "Are you sure you want to stop reporting today?",
    emptyAnswerAlert: "Please answer all the questions"
  },
  map: {
    confirmed: "Confirmed",
    recovered: "Recovered",
    death: "Deaths"
  },
  bottomMenu: {
    home: "Home",
    globalMap: "Global Map",
    localMap: "Local Map",
    reportNow: "Report now",
  }
};
