import i18n from 'i18n-js';
import en from './en';
import vi from './vi';
import ja from './ja';
import ru from './ru';
import si from './si';
import ta from './ta';
import pt from './pt';
import * as Localization from 'expo-localization';

i18n.translations = {
  en: en,
  vi: vi,
  ja: ja,
  ru: ru,
  si: si,
  ta: ta,
  pt: pt
};
i18n.locale = Localization.locale.slice(0, 2);
i18n.fallbacks = true;

export default i18n;