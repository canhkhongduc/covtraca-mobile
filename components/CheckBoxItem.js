import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {CheckBox} from 'react-native-elements';
import i18n from '../localization/i18n';

export default function CheckBoxItem({item, onChangeValue, checked}) {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{i18n.t(`reporting.${item.i18n}`)}</Text>
      <CheckBox
        checked={checked}
        onIconPress={() => onChangeValue(item.id)}
        style={styles.checkbox}
        size={26}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingLeft: 15,
    width: '80%',
    flexDirection: 'row',
    alignItems:'center'
  },
  checkbox: {
    flex: 1,
    marginLeft: 10
  },
  title: {
    flex: 5,
    fontSize: 15,
  }
});