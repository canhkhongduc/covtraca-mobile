import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import RadioForm from 'react-native-simple-radio-button';
import Colors from '../constants/Colors';
import commonStyles from '../constants/commonStyles';
import * as Progress from 'react-native-progress';
import i18n from '../localization/i18n';

export default function SelectQuestion({question, progress, onChangeValue}) {
  for(let i = 0; i < question.options.length; i++) {
    question.options[i].label = i18n.t(`reporting.${question.options[i].i18n}`);
  }
  return (
    <View>
      <Text style={styles.title}>{i18n.t(`reporting.${question.i18n}`)}</Text>
      <RadioForm
        radio_props={question.options}
        initial={-1}
        onPress={(value) => onChangeValue(question.id, value)}
        animation={true}
        buttonSize={14}
        buttonOuterSize={25}
        labelStyle={{fontSize: 15, marginRight: 15}}
        buttonColor={Colors.black}
        labelColor={Colors.black}
        style={styles.radioButtonContainer}
      />
    </View>
    
  );
}

const styles = StyleSheet.create({
  radioButtonContainer: {
    marginTop: 8
  },
  title: {
    fontSize: 17,
		color: Colors.black,
    backgroundColor: 'transparent',
    marginTop: 30
  }
});