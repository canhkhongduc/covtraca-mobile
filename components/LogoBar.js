import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import Layout from "../constants/Layout";
import Colors from '../constants/Colors';

const width = Layout.window.width;

export default function LogoBar(props) {
  return (
    <View style={styles.logoBar}>
      <Image 
        source={require('../assets/images/logo-header.png')}
        style={styles.logoHeader}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  logoBar: {
		height: 75,
		width: width,
		backgroundColor: Colors.primaryColor,
		flexDirection: 'row',
		justifyContent: 'flex-end',
	},
	logoHeader: {
		width: '50%',
		marginTop: 20,
		marginRight: 10,
		resizeMode: 'contain',
	}
});