import actionTypes from '../constants/actionTypes/question';

const INITIAL_STATE = {
  questions: [],
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case actionTypes.GET_QUESTIONS:
      return {
        ...state,
        questions: payload,
      };
    default:
      return state;
  }
};
