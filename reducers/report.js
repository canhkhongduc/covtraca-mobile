import actionTypes from '../constants/actionTypes/report';

const INITIAL_STATE = {
  report: {},
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case actionTypes.SAVE_REPORT:
      return {
        ...state,
        report: payload,
      };
    default:
      return state;
  }
};
