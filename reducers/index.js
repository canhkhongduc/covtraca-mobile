import { combineReducers } from 'redux';
import symptomReducer from './symptom';
import questionReducer from './question';
import deviceReducer from './device';
import reportReducer from './report';
import summaryReducer from './covidSummary';

export default combineReducers({
    symptom: symptomReducer,
    question: questionReducer,
    device: deviceReducer,
    report: reportReducer,
    summary: summaryReducer
});