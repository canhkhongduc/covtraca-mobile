import actionTypes from '../constants/actionTypes/device';

const INITIAL_STATE = {
  device: {},
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case actionTypes.GET_DEVICE_BY_UID:
      return {
      ...state,
      device: payload,
      };
    case actionTypes.REGISTER_DEVICE:
      return {
        ...state,
        device: payload,
      };
    default:
      return state;
  }
};
