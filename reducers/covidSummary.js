import actionTypes from '../constants/actionTypes/covidSummary';

const INITIAL_STATE = {
  summary: {},
  local: {},
  dailyReport: {}
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case actionTypes.GET_COVID_SUMMARY:
      return {
        ...state,
        summary: payload,
      };
    case actionTypes.GET_COVID_LOCAL:
      return {
        ...state,
        local: payload,
      };
    case actionTypes.GET_COVID_LOCAL_DAILY:
      return {
        ...state,
        dailyReport: payload,
      };
        
    default:
      return state;
  }
};
