import actionTypes from '../constants/actionTypes/symptom';

const INITIAL_STATE = {
  symptoms: {},
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case actionTypes.SET_IS_COVID_TESTED:
      return {
        ...state,
        isTested: payload,
      };
    case actionTypes.SET_CURRENT_HEALTH:
      return {
        ...state,
        currentHealth: payload,
      };
    case actionTypes.UPDATE_SYMPTOMS:
      return {
        ...state,
        symptoms: payload,
      };
    case actionTypes.SET_CURRENT_PLACE:
      return {
        ...state,
        currentPlace: payload,
      };
    case actionTypes.SET_TREATMENT:
      return {
        ...state,
        treatment: payload,
      };
    default:
      return state;
  }
};
