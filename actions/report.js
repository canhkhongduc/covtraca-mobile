import actionTypes from '../constants/actionTypes/report';
import * as apiRequest from '../utils/apiRequest';
import * as endpoints from '../constants/endpoints';

export function saveReport(report) {
  return async(dispatch) => {
      try {
        const res = await apiRequest.fetchWithoutToken(endpoints.saveReport, 'POST', report);
        dispatch({type: actionTypes.SAVE_REPORT, payload: res.data });
      } catch (err) {
        console.log(err)
      }
  }
}