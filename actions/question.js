import actionTypes from '../constants/actionTypes/question';
import * as apiRequest from '../utils/apiRequest';
import * as endpoints from '../constants/endpoints';

export function getQuestions() {
  return async(dispatch) => {
      try {
        const res = await apiRequest.fetchWithoutToken(endpoints.getAllQuestions, 'GET');
        dispatch({type: actionTypes.GET_QUESTIONS, payload: res.data });
      } catch (err) {
        console.log(err)
      }
  }
}