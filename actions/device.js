import actionTypes from '../constants/actionTypes/device';
import * as apiRequest from '../utils/apiRequest';
import * as endpoints from '../constants/endpoints';

export function registerDevice(device) {
  return async(dispatch) => {
      try {
        const res = await apiRequest.fetchWithoutToken(endpoints.registerDevice, 'POST', device);
        dispatch({type: actionTypes.REGISTER_DEVICE, payload: res.data });
      } catch (err) {
        console.log(err)
      }
  }
}

export function getDeviceByUid(uid) {
  return async(dispatch) => {
      try {
        const res = await apiRequest.fetchWithoutToken(endpoints.getDeviceByUid(uid), 'GET');
        dispatch({type: actionTypes.GET_DEVICE_BY_UID, payload: res.data });
      } catch (err) {
        console.log(err)
      }
  }
}