import actionTypes from '../constants/actionTypes/covidSummary';
import * as apiRequest from '../utils/apiRequest';
import * as endpoints from '../constants/endpoints';

export function getCovidData() {
  return async(dispatch) => {
    try {
      const res = await apiRequest.fetchWithoutToken(endpoints.getCovidSummary, 'GET');
      dispatch({type: actionTypes.GET_COVID_SUMMARY, payload: res });
    } catch (err) {
      console.log(err)
    }
  }
}

export function getCovidLocalData(countryCode) {
  return async(dispatch) => {
    try {
      const res = await apiRequest.fetchWithoutToken(endpoints.getCovidLocal(countryCode), 'GET');
      dispatch({type: actionTypes.GET_COVID_LOCAL, payload: res });
    } catch (err) {
      console.log(err)
    }
  }
}

export function getCovidLocalDailyData(countryCode) {
  return async(dispatch) => {
    try {
      const res = await apiRequest.fetchWithoutToken(endpoints.getCovidLocalDaily(countryCode), 'GET');
      dispatch({type: actionTypes.GET_COVID_LOCAL_DAILY, payload: res });
    } catch (err) {
      console.log(err)
    }
  }
}