import { AsyncStorage } from 'react-native';
import actionTypes from '../constants/actionTypes/symptom';

export const setIsTested = (isTested) => async dispatch => {
  dispatch({type: actionTypes.SET_IS_COVID_TESTED, payload: isTested });
}

export const setCurrentHealth = (currentHealth) => async dispatch => {
  dispatch({type: actionTypes.SET_CURRENT_HEALTH, payload: currentHealth });
}

export const updateSymptoms = (symptoms) => async dispatch => {
  dispatch({type: actionTypes.UPDATE_SYMPTOMS, payload: symptoms });
}

export const setCurrentPlace = (currentPlace) => async dispatch => {
  dispatch({type: actionTypes.SET_CURRENT_PLACE, payload: currentPlace });
}

export const setTreatment = (treatment) => async dispatch => {
  dispatch({type: actionTypes.SET_TREATMENT, payload: treatment });
}